Final Project
Kunal Maiti and Michael Oluwole
JHED IDs: kmaiti1 and moluwol1
Phone numbers: 667 207 4063 and 468 236 5098
Emails: kmaiti1@jhu.edu and moluwol1@jhu.edu
Last modified on 2016-12-05

The cs120final folder has the following files and folders:
data (folder)
include (folder)
src (folder)
bin (folder)
filenames.txt
README.txt
gitlog.txt

The 'data' folder has the following files:
file1.txt and file2.txt - These text files are used only for the unit tests.

The 'src' folder has the following files and folders:
obj (folder) - All object files are placed here when they get made
Makefile - This makefile works for both the driver and the unit tests
Driver.cpp - The driver file with the main function
LanguageModel.cpp
NgramCollection.cpp
LMCollection.cpp
unitTestDriver.cpp
unitTests-NgramCollection.cpp

The 'include' folder has the following files:
catch.hpp
NgramCollection.hpp
LanguageModel.hpp
LMCollection.hpp

The 'bin' folder is where you will find the driver and unitTests executable files
once compilation is complete.
-----------------------------------------------

1) Running. (How to build and run our program:)
Please use the included makefile.
On the terminal, use the cd command to get into the src directory, and then simply type "make".
After compilation, run the driver file in the bin folder to run the program or the unitTests to run the unit tests.

2) Testing. (How to run tests for each class and/or module/function in our program:)
You can use the UnitTestDriver and unitTests-NgramCollection files
to run unit tests (included) for the new LMCollection class and so forth.

3) Plan of Attack:
In order to detect plagiarism between 2 text files, our program makes use of ngrams.
Our LMCollection class constructor takes an int called 'scrutiny' corresponding to the n in our n-grams.
The LMCollection class then makes a language model from a given file, for each file.
The test method then compares Language Models in the LMCollection.
The results of these comparisons decide whether the 2 text files compared were plagiarised or not.

4) Design. (What design choices we made:)
Source files:
Our program has 5 important parts:
1) Driver: This takes in the value of the sensitivity flag and the filename of the file containing filenames for the text files
to be checked for plagiarism. This is also where we create our LMCollection (see below) object to actually make the comparisons
between the Ngrams created for each file.
2) NgramCollection: This is mostly unchanged from the version given in the Assignment 8 solution by the professors.
3) LanguageModel: Identical to the LanguageModel class given in the Assignment 8 Solution, EXCEPT we added some functions to facilitate
comparisons between LanguageModels.
4) LMCollection: An all new class created by us. As the name suggests, this class makes use of LanguageModels and
makes collections of them to facilitate comparing them to find plagiarism.
5) unitTests-NgramCollection: Identical to the unit tests given in the Assignment 8 Solution, EXCEPT we added some
important unit tests for the new LMCollection class. We have basically tested every function of the LMCollection class,
including the constructor. The text files in the data folder are used for the unit tests.

5) Flags. (What the sensitivity flags mean (i.e. what they actually do for our program):)
The sensitivity flags are implemented using the variable "scrutiny" in Driver.cpp.
Depending on the value of scrutiny, the value of n in our ngrams is modified to change the size of the ngrams.
The larger the value of n, the lower the sensitivity.
The idea for plagiarism detection with n-grams is that, as you increase n,
the probability that a given n-gram will be shared between two arbitrary documents rapidly vanishes.

6)  Timing. (How long your program should take to run on the provided datasets
(or a note that it is unable to run to completion on a given dataset):)
The program runs to completion on all datasets. :D
It may however take a bit of time with the large dataset.

7) Challenges.
- Deciding how to implement the sensitivity flags
- Iterating through the LanguageModels
Unforeseeable changes needed to be made to the LanguageModel class itself in order for it to work 
with our LMCollection class (collection of language models), like adding a 'contains' method and 'compare' 
method. 
In order to test plagiarism we want to test frequency of similar sentences and phrases. But we needed to decide what the
threshold for the frequency would be, past which the pair of files would raise suspicion. 15% seemed like a good amount that should be suspicious. 
Another problem we faced was finding an efficient way to process the files and construct the LangaugeModels. We didn't want to process each file multiple times,
so we processed the files once and then stored them and compaired every possible pair of files greatly reducing the time for the program to run.

8) Future.
If we had more time, we would have tried to make the program more efficient so that it would be able to handle even
more files and do everything faster.
We also would have liked to make the sensitivity flags more effective.
Also, our program just shows the suspect document to indicate that it shares n-grams with the source documents, but in the future
we could perhaps annotate exactly which parts of the document to look for.
Our program doesn't account for location of the words we find that are the same in each file. Therefore, it doesn't capture the context within which a phrase was 
stated. This can cause incorrect plaigarism flags. We would have liked to analyze sentence structure by tagging the file with POS tags.
