/*
Final Project
Kunal Maiti and Michael Oluwole
JHED IDs: kmaiti1 and moluwol1
Phone numbers: 667 207 4063 and 468 236 5098
Emails: kmaiti1@jhu.edu and moluwol1@jhu.edu
Last modified on 2016-12-05
*/

#ifndef _LMCOLLECTION_HPP
#define _LMCOLLECTION_HPP
#include <vector>
#include "LanguageModel.hpp"
class LMCollection {

public:

  // constructor takes a scrutiny corresponding to N, the number of word grams
  LMCollection(std::string scrutiny) {
    if( scrutiny == "h") {
      n = 3;
    } else if(scrutiny == "m") {
      n = 5;
    } else if(scrutiny == "l") {
      n = 8;
    }
  }

  // read file to find file list
  void readFileList(std::string fname);

  // Make a language model from a given file
  void addLanguageModel(std::string fname);

  // compare Language Models
  void test();

  //Retrieve the value for N
  int getN() const { return n; }

private:
   // read a list of paths from a file
  std::vector<std::string> readNames(std::string listFile);

  // n value for language models
  int n;

  // stores LM for each file
  std::vector<std::tuple<LanguageModel,std::string>> languageModels;

};

#endif
