/*
Final Project
Kunal Maiti and Michael Oluwole
JHED IDs: kmaiti1 and moluwol1
Phone numbers: 667 207 4063 and 468 236 5098
Emails: kmaiti1@jhu.edu and moluwol1@jhu.edu
Last modified on 2016-12-05
*/

#include "LMCollection.hpp"
#include <iostream>
#include <tuple>
#include <fstream>



// Read and build vector of LanguageModels
void LMCollection :: readFileList(std::string fname) {
  std::vector<std::string> nameList = readNames(fname);
  for(std::string &name : nameList) {
    addLanguageModel(name);
  }
}

/* read a list of names from a file */
std::vector<std::string> LMCollection :: readNames(std::string listFile) {
  std::vector<std::string> names;
  std::ifstream fin(listFile); // try to open file
  if (!fin.is_open()) { // see if it worked
    std::cerr << "Error: failed to open listfile '" << listFile << "'\n";
    exit(-1);
  }
  std::string name;
  while (fin >> name) {
    names.push_back(name); // read names until we run out of file
  }
  fin.close(); // close the file
  return names;
}

// Build and add a language model to the Collection
void LMCollection :: addLanguageModel(std::string fname) {
  LanguageModel lm(n);
  lm.addTextFromFile(fname);
  std::tuple<LanguageModel,std::string> pair = std::make_tuple(lm,fname);
  languageModels.push_back(pair);
}


// Test the language models in the collection
void LMCollection :: test() {
  for(unsigned i = 0; i < languageModels.size(); i++) {
    for(unsigned j = i+1;j < languageModels.size(); j++) {
      if((std::get<0>(languageModels.at(i))).compare(std::get<0>(languageModels.at(j))) > .15) {
        std::cout << std::get<1>(languageModels.at(i)) << " " << std::get<1>(languageModels.at(j)) << std::endl;
      }
    }
  }
}
