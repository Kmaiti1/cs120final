/*
Final Project
Kunal Maiti and Michael Oluwole
JHED IDs: kmaiti1 and moluwol1
Phone numbers: 667 207 4063 and 468 236 5098
Emails: kmaiti1@jhu.edu and moluwol1@jhu.edu
Last modified on 2016-12-05
*/

#include <iostream>
#include <sstream>
#include <string>
#include "LMCollection.hpp"
#include <fstream>
#include <vector>
#include <exception>
#include <stdexcept>

/* function to print how to call this program */
void printUsage() {
    std::cout << "Usage: a8 [input file] [l|m|h] \n";
}

/* main: parse arguments, call appropriate functions */
int main(int argc, char** argv) {

  if (argc < 2) {
    printUsage();
    return 1;
  }

  std::string inputName(argv[1]);
  std::string scrutiny;

  if (argc < 3) {
    scrutiny = "m";
  } else {
    scrutiny = std::string(argv[2]);
  }
  if(scrutiny != "l" && scrutiny != "m" && scrutiny != "h") {
    printUsage();
    return 1;
  }

  LMCollection lm(scrutiny);
  lm.readFileList(inputName);

  lm.test();

  return 0;
}
